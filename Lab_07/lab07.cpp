#if defined(__linux__)						// If we are using linux.

#include <SDL2/SDL.h>
#include <GL/glew.h>
#include <SDL2/SDL_opengl.h>

#elif defined(_WIN16) || defined(_WIN32) || defined(_WIN64)	// If we are using a windows machine
#include <SDKDDKVer.h>

//Using SDL, SDL OpenGL, GLEW, standard IO, and strings
#include <SDL.h>
#include <gl\glew.h>
#include <SDL_opengl.h>
#include <gl\glu.h>
#endif


#define GLM_FORCE_RADIANS //Using degrees with glm is deprecated.
#include <glm/glm.hpp>
#include <glm/gtc/constants.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include <stdio.h>
#include <string>

#include "ShaderHandler.h"
#include "vertexData.h"
#include "util.h"
#include "globals.h"


bool init()
{
	//Initialization flag
	bool success = true;

	//Initialize SDL
	if (SDL_Init(SDL_INIT_VIDEO) < 0)
	{
		printf("SDL could not initialize! SDL Error: %s\n", SDL_GetError());
		success = false;
	}
	else
	{
		//Ask SDL to give us a core context.
		SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);

		//Create window
		gWindow = SDL_CreateWindow("SDL Tutorial", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN);
		if (gWindow == NULL)
		{
			printf("Window could not be created! SDL Error: %s\n", SDL_GetError());
			success = false;
		}
		else
		{
			//Create context
			gContext = SDL_GL_CreateContext(gWindow);
			if (gContext == NULL)
			{
				printf("OpenGL context could not be created! SDL Error: %s\n", SDL_GetError());
				success = false;
			}
			else
			{
				//Initialize GLEW
				glewExperimental = GL_TRUE;
				GLenum glewError = glewInit();
				if (glewError != GLEW_OK)
				{
					printf("Error initializing GLEW! %s\n", glewGetErrorString(glewError));
				}

				//Use Vsync
				if (SDL_GL_SetSwapInterval(1) < 0)
				{
					printf("Warning: Unable to set VSync! SDL Error: %s\n", SDL_GetError());
				}

				//Initialize OpenGL
				if (!initGL())
				{
					printf("Unable to initialize OpenGL!\n");
					success = false;
				}
			}
		}
	}

	return success;
}

bool initGL()
{
	//Success flag
	bool success = true;

	// Enable depth test
	glEnable(GL_DEPTH_TEST);
	// Accept fragment if it closer to the camera than the former one
	glDepthFunc(GL_LESS);

	currentShaderProgram = shaderHandler.initializeShaders();

	// Projection matrix : Field of View, screen size as the ratio, display range : 0.1 unit <-> 100 units
	gProjectionMatrix = glm::perspective(glm::quarter_pi<float>() * 1.5f, static_cast<float>(SCREEN_WIDTH) / static_cast<float>(SCREEN_HEIGHT), 0.1f, 200.0f);

	// Camera matrix
	gViewMatrix = glm::lookAt(
		gCameraPosition, // Camera position in World Space
		glm::vec3(0.0f, 0.0f, 0.0f), // and looks at the origin
		glm::vec3(0.0f, 1.0f, 0.0f)  // Head is up (set to 0,-1,0 to look upside-down)
		);

	gModelMatrixCube = glm::mat4(2.0f);
	gModelMatrixFloor = glm::mat4(1.0f);

	// Our ModelViewProjection : multiplication of our 3 matrices
	gMVPMatrixCube = gProjectionMatrix * gViewMatrix * gModelMatrixCube; // Remember, matrix multiplication is the other way around


	//CUBE
	num_vertices = gNumVerticesCube;
	num_indices = gNumIndicesCube;

	//VAO
	glGenVertexArrays(1, &gVAOCube);
	glBindVertexArray(gVAOCube);

	//Create VBO
	glGenBuffers(1, &gVBOCube);
	glBindBuffer(GL_ARRAY_BUFFER, gVBOCube);
	glBufferData(GL_ARRAY_BUFFER, sizeof(gVertexDataCube)* sizeof(GLfloat), gVertexDataCube, GL_STATIC_DRAW);

	//Create IBO
	glGenBuffers(1, &gIBOCube);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gIBOCube);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(gIndexDataCube)* sizeof(GLuint), gIndexDataCube, GL_STATIC_DRAW);

	//Enable vertex data
	glEnableVertexAttribArray(0); //attribute 0 is for vertex position data
	glEnableVertexAttribArray(1); //attribute 1 is for vertex color data
	glEnableVertexAttribArray(2); //attribute 2 is for vertex texture data
	glEnableVertexAttribArray(3); //attribute 3 is for vertex normal data

	//Set vertex data
	glVertexAttribPointer(0, vertex_order, GL_FLOAT, GL_FALSE, vertex_order * sizeof(GLfloat), NULL);
	glVertexAttribPointer(1, color_depth, GL_FLOAT, GL_FALSE, color_depth * sizeof(GLfloat), (void*)(num_vertices*vertex_order*sizeof(GL_FLOAT))); //color starts after the vertex data
	glVertexAttribPointer(2, texture_order, GL_FLOAT, GL_FALSE, texture_order * sizeof(GLfloat), (void*)(num_vertices*vertex_order*sizeof(GL_FLOAT) + num_vertices * color_depth*sizeof(GL_FLOAT))); //normal data starts after the color data
	glVertexAttribPointer(3, normal_order, GL_FLOAT, GL_FALSE, normal_order * sizeof(GLfloat), (void*)(num_vertices*vertex_order*sizeof(GL_FLOAT) + num_vertices * color_depth*sizeof(GL_FLOAT) + num_vertices * normal_order*sizeof(GL_FLOAT))); //normal data starts after the color data

	//FLOOR
	num_vertices = gNumVerticesFloor;
	num_indices = gNumIndicesFloor;

	//VAO
	glGenVertexArrays(1, &gVAOFloor);
	glBindVertexArray(gVAOFloor);

	//Create VBO
	glGenBuffers(1, &gVBOFloor);
	glBindBuffer(GL_ARRAY_BUFFER, gVBOFloor);
	glBufferData(GL_ARRAY_BUFFER, sizeof(gVertexDataFloor)* sizeof(GLfloat), gVertexDataFloor, GL_STATIC_DRAW);

	//Create IBO
	glGenBuffers(1, &gIBOFloor);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gIBOFloor);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(gIndexDataFloor)* sizeof(GLuint), gIndexDataFloor, GL_STATIC_DRAW);

	//Enable vertex data
	glEnableVertexAttribArray(0); //attribute 0 is for vertex position data
	glEnableVertexAttribArray(1); //attribute 1 is for vertex color data
	glEnableVertexAttribArray(2); //attribute 2 is for vertex texture data
	glEnableVertexAttribArray(3); //attribute 3 is for vertex normal data

	//Set vertex data
	glVertexAttribPointer(0, vertex_order, GL_FLOAT, GL_FALSE, vertex_order * sizeof(GLfloat), NULL);
	glVertexAttribPointer(1, color_depth, GL_FLOAT, GL_FALSE, color_depth * sizeof(GLfloat), (void*)(num_vertices*vertex_order*sizeof(GL_FLOAT))); //color starts after the vertex data
	glVertexAttribPointer(2, texture_order, GL_FLOAT, GL_FALSE, texture_order * sizeof(GLfloat), (void*)(num_vertices*vertex_order*sizeof(GL_FLOAT) + num_vertices * color_depth*sizeof(GL_FLOAT))); //normal data starts after the color data
	glVertexAttribPointer(3, normal_order, GL_FLOAT, GL_FALSE, normal_order * sizeof(GLfloat), (void*)(num_vertices*vertex_order*sizeof(GL_FLOAT) + num_vertices * color_depth*sizeof(GL_FLOAT) + num_vertices * normal_order*sizeof(GL_FLOAT))); //normal data starts after the color data

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);

	return success;
}

/**
* Rotates the camera using the passed in vector.
*
* NOTE! This rotation is only suitable for very simple camera rotation about the origin.
* @param rotationVector the vector used for rotation the about the x, y and z axis.
*/
void rotateCamera(const glm::vec3& rotationVector)
{
	gViewMatrix = glm::rotate(gViewMatrix, glm::length(rotationVector), rotationVector);
}

/** 
 * Rotates the model matrix
 */
void rotateModel()
{
	const glm::vec3 rotationVector = glm::vec3(1.0f, 1.0f, 0.0f);
	const float rotationSpeed = 0.01f;

	gModelMatrixCube = glm::rotate(gModelMatrixCube, rotationSpeed, rotationVector);
}

bool x = true;
bool y = true;
bool z = true;
void changeLightColor(float step) {
	if(gLightColor.x >= 1.0f || gLightColor.x <= 0.0f) {
		x = !x;
	}

	if(x) {
		gLightColor.x += step;
	} else {
		gLightColor.x -= step;
	}


	if(gLightColor.y >= 1.0f || gLightColor.y <= 0.0f) {
		y = !y;
	}

	if(y) {
		gLightColor.y += step*2;
	} else {
		gLightColor.y -= step*2;
	}

	if(gLightColor.z >= 1.0f || gLightColor.z <= 0.0f) {
		z = !z;
	}

	if(z) {
		gLightColor.z += step*3;
	} else {
		gLightColor.z -= step*3;
	}

}

void update()
{
	if (gAutomaticRotation == true)
	{
		rotateCamera(glm::vec3(0.0f, 0.02f, 0.0f));
	}

	rotateModel();

	gMVPMatrixCube = gProjectionMatrix * gViewMatrix * gModelMatrixCube;
	gMVPMatrixFloor = gProjectionMatrix * gViewMatrix * gModelMatrixFloor;

	changeLightColor(0.003f);
}

void renderQuad() {
	//Render quad
	if (gRenderQuad)
	{
		//Bind program
		glUseProgram(currentShaderProgram->programId);

		//Bind Cube
		glBindVertexArray(gVAOCube); 
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gIBOCube);

		// Send our transformation to the currently bound shader, 
		// in the "MVP" uniform
		glUniformMatrix4fv(currentShaderProgram->MVPId, 1, GL_FALSE, glm::value_ptr(gMVPMatrixCube));		//Note that glm::value_ptr(gMVPMatrix) is the same as &gMVPMatrix[0][0]]
		glUniformMatrix4fv(currentShaderProgram->viewMatrixId, 1, GL_FALSE, glm::value_ptr(gViewMatrix));
		glUniformMatrix4fv(currentShaderProgram->modelMatrixId, 1, GL_FALSE, glm::value_ptr(gModelMatrixCube));

		glUniform4f(currentShaderProgram->lightPositionId, gLightPosition.x, gLightPosition.y, gLightPosition.z, 1.0f);

		glUniform4fv(currentShaderProgram->lightColorId, 1, glm::value_ptr(gLightColor));
		glUniform4fv(currentShaderProgram->materialDiffuseColorId, 1, glm::value_ptr(gMaterialCubeDiffuseColor));
		glUniform4fv(currentShaderProgram->materialSpecularColorId, 1, glm::value_ptr(gMaterialCubeSpecularColor));

		//Draw Cube
		glDrawElements(GL_TRIANGLES, gNumVerticesCube, GL_UNSIGNED_INT, NULL);

		//Unbind program
		glUseProgram(NULL);
	}
}

void renderFloor() {
	//ShaderHandler::ShaderProgram* tempProgram = currentShaderProgram;
	//currentShaderProgram = shaderHandler.getShader("DiffuseLighting");

	//Bind program
	glUseProgram(currentShaderProgram->programId);

	//Bind Floor
	glBindVertexArray(gVAOFloor); 
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gIBOFloor);

	// Send our transformation to the currently bound shader, 
	// in the "MVP" uniform
	glUniformMatrix4fv(currentShaderProgram->MVPId, 1, GL_FALSE, glm::value_ptr(gMVPMatrixFloor));		//Note that glm::value_ptr(gMVPMatrix) is the same as &gMVPMatrix[0][0]]
	glUniformMatrix4fv(currentShaderProgram->viewMatrixId, 1, GL_FALSE, glm::value_ptr(gViewMatrix));
	glUniformMatrix4fv(currentShaderProgram->modelMatrixId, 1, GL_FALSE, glm::value_ptr(gModelMatrixFloor));

	glUniform4f(currentShaderProgram->lightPositionId, gLightPosition.x, gLightPosition.y, gLightPosition.z, 1.0f);

	glUniform4fv(currentShaderProgram->lightColorId, 1, glm::value_ptr(gLightColor));
	glUniform4fv(currentShaderProgram->materialDiffuseColorId, 1, glm::value_ptr(gMaterialFloorDiffuseColor));
	glUniform4fv(currentShaderProgram->materialSpecularColorId, 1, glm::value_ptr(gMaterialFloorSpecularColor));

	//Draw Floor
	glDrawElements(GL_TRIANGLE_FAN, gNumVerticesFloor, GL_UNSIGNED_INT, NULL);

	//Unbind VAO
	glBindVertexArray(0);

	//Unbind program
	glUseProgram(NULL);

	//currentShaderProgram = tempProgram;
}

void render() {
	glClearColor(0.2f, 0.2f, 0.2f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	renderQuad();
	renderFloor();

	//Update screen
	SDL_GL_SwapWindow(gWindow);
}

int main(int argc, char* args[])
{
	//Start up SDL and create window
	if (!init())
	{
		printf("Failed to initialize!\n");
	}
	else
	{
		//Main loop flag
		gRunning = true;

		//Event handler
		SDL_Event eventHandler;

		//Enable text input
		SDL_StartTextInput();

		//While application is running
		while (gRunning)
		{
			//Handle events
			events(eventHandler);

			//Handle updates
			update();

			//Render quad
			render();
		}

		//Disable text input
		SDL_StopTextInput();
	}

	//Free resources and close SDL
	close();

	return 0;
}