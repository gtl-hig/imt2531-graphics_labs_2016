#include "util.h"
#include "globals.h"

void handleKeys(unsigned char key, int x, int y)
{
	//Toggle cube
	if (key == 'q')
	{
		gRenderQuad = !gRenderQuad;
	}
	else if (key == 'r')
	{
		gAutomaticRotation = !gAutomaticRotation;
	}
	else if (key == '1')
	{
		currentShaderProgram = shaderHandler.getShader("Color");
	}
	else if (key == '2')
	{
		currentShaderProgram = shaderHandler.getShader("DiffuseLighting");
	}
	else if (key == '3')
	{
		currentShaderProgram = shaderHandler.getShader("FullLightingNoTextureOrColor");
	}
	else if (key == '4')
	{
		currentShaderProgram = shaderHandler.getShader("FullLightingNoTextureOrColorV2");
	}
}

void events(SDL_Event& eventHandler)
{
	//Handle events on queue
	while (SDL_PollEvent(&eventHandler) != 0)
	{
		//User requests quit
		if (eventHandler.type == SDL_QUIT)
		{
			gRunning = false;
		}
		//Handle keypress with current mouse position
		else if (eventHandler.type == SDL_TEXTINPUT)
		{
			int x = 0, y = 0;
			SDL_GetMouseState(&x, &y);
			handleKeys(eventHandler.text.text[0], x, y);
		}
	}
}

void close()
{
	//Deallocate program
	glDeleteProgram(currentShaderProgram->programId);

	//Destroy window	
	SDL_DestroyWindow(gWindow);
	gWindow = NULL;

	//Quit SDL subsystems
	SDL_Quit();
}

void printProgramLog(GLuint program)
{
	//Make sure name is shader
	if (glIsProgram(program))
	{
		//Program log length
		int infoLogLength = 0;
		int maxLength = infoLogLength;

		//Get info string length
		glGetProgramiv(program, GL_INFO_LOG_LENGTH, &maxLength);

		//Allocate string
		char* infoLog = new char[maxLength];

		//Get info log
		glGetProgramInfoLog(program, maxLength, &infoLogLength, infoLog);
		if (infoLogLength > 0)
		{
			//Print Log
			printf("%s\n", infoLog);
		}

		//Deallocate string
		delete[] infoLog;
	}
	else
	{
		printf("Name %d is not a program\n", program);
	}
}

void printShaderLog(GLuint shader)
{
	//Make sure name is shader
	if (glIsShader(shader))
	{
		//Shader log length
		int infoLogLength = 0;
		int maxLength = infoLogLength;

		//Get info string length
		glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &maxLength);

		//Allocate string
		char* infoLog = new char[maxLength];

		//Get info log
		glGetShaderInfoLog(shader, maxLength, &infoLogLength, infoLog);
		if (infoLogLength > 0)
		{
			//Print Log
			printf("%s\n", infoLog);
		}

		//Deallocate string
		delete[] infoLog;
	}
	else
	{
		printf("Name %d is not a shader\n", shader);
	}
}