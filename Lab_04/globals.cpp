#include "globals.h"

//Screen dimension constants
const int SCREEN_WIDTH = 640;
const int SCREEN_HEIGHT = 480;

//The window we'll be rendering to
SDL_Window* gWindow = NULL;

//OpenGL context
SDL_GLContext gContext;

//Render flag
bool gRenderQuad = true;

//Graphics program
GLuint gProgramID = 0;
GLuint gVBO = 0;
GLuint gIBO = 0;
GLuint gVAO = 0;
int num_vertices=0;
int vertex_order = 4; // we are using x,y,z,w vertices
int color_depth = 4;  // 4 bytes is 32 bit color