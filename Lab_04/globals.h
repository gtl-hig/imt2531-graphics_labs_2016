#pragma	once

#include <SDL2/SDL.h>
#include <GL/glew.h>
#include <SDL2/SDL_opengl.h>
#include <GL/glu.h>

//Screen dimension constants
extern const int SCREEN_WIDTH;
extern const int SCREEN_HEIGHT;

//The window we'll be rendering to
extern SDL_Window* gWindow;

//OpenGL context
extern SDL_GLContext gContext;

//Render flag
extern bool gRenderQuad;

//Graphics program
extern GLuint gProgramID;
extern GLuint gVBO;
extern GLuint gIBO;
extern GLuint gVAO;
extern int num_vertices;
extern int vertex_order;
extern int color_depth;