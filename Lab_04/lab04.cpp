
#include <SDL2/SDL.h>
#include <GL/glew.h>
#include <SDL2/SDL_opengl.h>
#include <GL/glu.h>

#include <stdio.h>
#include <string>
#include "util.h"
#include "globals.h"

//Per frame update
void update()
{
	//No per frame update needed
}

//Renders quad to the screen
void render()
{
	//Clear color buffer
	glClear(GL_COLOR_BUFFER_BIT);

	//Render quad
	if (gRenderQuad)
	{
		//Bind program
		glUseProgram(gProgramID);
		glBindVertexArray(gVAO); //*
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gIBO);
/* 		This Code has been moved to the initBuffers() function in util.cpp to take advantage of VertexArrayObjects
		To go back to the prious version comment out lines with //* after them both here and in initBuffers()
		//Enable vertex position
		glEnableVertexAttribArray(0); //attribute 0 is for vertex position data
		glEnableVertexAttribArray(1); //attribute 1 is for vertex color data

		//Set vertex data
		glBindBuffer(GL_ARRAY_BUFFER, gVBO);
		glVertexAttribPointer(0, vertex_order, GL_FLOAT, GL_FALSE, vertex_order* sizeof(GLfloat), NULL);
		glVertexAttribPointer(1, color_depth, GL_FLOAT, GL_FALSE, color_depth* sizeof(GLfloat),  (void*)(num_vertices*vertex_order*sizeof(GL_FLOAT))); //color data is 64 bytes in to the array

		//Set index data and render
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gIBO);
*/
		glDrawElements(GL_TRIANGLE_FAN, num_vertices, GL_UNSIGNED_INT, NULL); // you can draw other elements here. GL_LINE_LOOP
/*
		//Disable vertex position
		glDisableVertexAttribArray(0);
		glDisableVertexAttribArray(1);
*/
		//Unbind program
		glBindVertexArray(0);  //*
		glUseProgram(NULL);
	}
}


int main(int argc, char* args[])
{
	//Start up SDL and create window
	if (!init())
	{
		printf("Failed to initialize!\n");
	}
	else
	{
		//Main loop flag
		bool running = true;

		//Event handler
		SDL_Event e;

		//Enable text input
		SDL_StartTextInput();

		//While application is running
		while (running)
		{
			//Handle events on queue
			while (SDL_PollEvent(&e) != 0)
			{
				//User requests quit
				if (e.type == SDL_QUIT)
				{
					running = false;
				}
				//Handle keypress with current mouse position
				else if (e.type == SDL_TEXTINPUT)
				{
					int x = 0, y = 0;
					SDL_GetMouseState(&x, &y);
					handleKeys(e.text.text[0], x, y);
				}
			}

			//Render quad
			render();

			//Update screen
			SDL_GL_SwapWindow(gWindow);
		}

		//Disable text input
		SDL_StopTextInput();
	}

	//Free resources and close SDL
	close();

	return 0;
}