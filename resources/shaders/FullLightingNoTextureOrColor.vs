#version 330 core

// Input vertex data, different for all executions of this shader.
layout(location = 0) in vec4 vertexPosition_modelspace;
layout(location = 3) in vec4 vertexNormal_modelspace;

// Output data ; will be interpolated for each fragment.
out vec4 vertexPosition_cameraspace;
out vec4 normal_cameraspace;
out vec4 lightPosition_cameraspace;

// Values that stay constant for the whole mesh.
uniform mat4 MVP;
uniform mat4 ViewMatrix;
uniform mat4 ModelMatrix;
uniform vec4 LightPosition_worldspace;

void main()
{		
	// Output position of the vertex, in clip space : MVP * position
	gl_Position =  MVP * vec4(vertexPosition_modelspace);

	// Vector that goes from the vertex to the camera, in camera space.
	// In camera space, the camera is at the origin (0,0,0).
	vertexPosition_cameraspace =  ViewMatrix * ModelMatrix * vertexPosition_modelspace;
		
	// Normal of the the vertex, in camera space
	normal_cameraspace = ViewMatrix * ModelMatrix * vertexNormal_modelspace;
	
	// The lights position in the camera space.
	lightPosition_cameraspace = ViewMatrix * LightPosition_worldspace;
}
