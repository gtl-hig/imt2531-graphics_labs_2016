#version 330 core

// Interpolated values from the vertex shaders
in vec4 fragmentTextureCoordinate;
in vec4 fragmentColor;

// The texture we are sampling from.
uniform sampler2D textureBuffer;

// Ouput data
out vec4 outputColor;

void main()
{
	// The texture function uses the texture and the interpolated coordinate
	// to find the color of the current fragment.
	vec4 textureColor = texture(textureBuffer, fragmentTextureCoordinate.xy);

	// The output color of each fragment is set to be the result of multiplying the vertex color with the texture color.
	outputColor = textureColor * fragmentColor;
}