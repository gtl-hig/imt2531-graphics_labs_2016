#version 330 core

// Interpolated values from the vertex shaders
in vec4 fragmentTextureCoordinate;
in vec4 fragmentColor;
in vec4 vertexPosition_cameraspace;
in vec4 normal_cameraspace;
in vec4 lightPosition_cameraspace;

// The texture we are sampling from.
uniform sampler2D textureBuffer;

// Ouput data
out vec4 outputColor;

// Light emission properties
// You probably want to put it as a uniform
vec4 lightColor = vec4(1.0, 1.0, 1.0, 1.0);

vec4 materialDiffuseColor = vec4(0.5, 0.5, 0.7, 1.0);
vec4 materialAmbientColor = materialDiffuseColor * 0.1;

void main()
{	
	
	
	// Normal of the computed fragment, in camera space
	vec4 n = normalize(normal_cameraspace);
	
	// Direction of the light (from the fragment to the light)
	vec4 lightDirection = lightPosition_cameraspace - vertexPosition_cameraspace;
	vec4 l = normalize(lightDirection);
	
	// Cosine of the angle between the normal and the light direction, 
	// clamped above 0
	//  - light is at the vertical of the triangle -> 1
	//  - light is perpendicular to the triangle -> 0
	//  - light is behind the triangle -> 0
	float normalLightDotProduct = dot(n, l);
	normalLightDotProduct = clamp(normalLightDotProduct, 0, 1);
	
	// The diffuse color depends on color of the light, 
	// the cosine of the angle difference between the normal and the light
	// and the diffuseColor.
	vec4 diffuseColor = lightColor * normalLightDotProduct * materialDiffuseColor;
	
	outputColor = materialAmbientColor + diffuseColor;	
}
