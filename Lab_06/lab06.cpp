#if defined(__linux__)						// If we are using linux.

#include <SDL2/SDL.h>
#include <GL/glew.h>
#include <SDL2/SDL_opengl.h>

#elif defined(_WIN16) || defined(_WIN32) || defined(_WIN64)	// If we are using a windows machine
#include <SDKDDKVer.h>

//Using SDL, SDL OpenGL, GLEW, standard IO, and strings
#include <SDL.h>
#include <gl\glew.h>
#include <SDL_opengl.h>
#include <gl\glu.h>
#endif

#define GLM_FORCE_RADIANS //Using degrees with glm is deprecated.
#include <glm/glm.hpp>
#include <glm/gtc/constants.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include <stdio.h>
#include <string>

#include "shader.hpp"
#include "globals.h"
#include "util.h"

//Per frame update
void update();

//Renders quad to the screen
void render();

/**
* Rotates the camera using the passed in vector.
*
* Also updates the gMVP matrix.
* NOTE! This rotation is only suitable for very simple camera rotation about the origin.
* @param rotationVector the vector used for rotation the about the x, y and z axis.
*/
void rotateCamera(const glm::vec3& rotationVector)
{
	glm::vec4 vector = gViewMatrix * glm::vec4(rotationVector, 0.0f);
	glm::vec3 rotvec = glm::vec3(vector.x, vector.y, vector.z);

	gViewMatrix = glm::rotate(gViewMatrix, glm::length(rotvec), rotvec);

	gMVPMatrix = gProjectionMatrix * gViewMatrix * gModelMatrix;
}

void events(SDL_Event& eventHandler)
{
	//Handle events on queue
	while (SDL_PollEvent(&eventHandler) != 0)
	{
		//User requests quit
		if (eventHandler.type == SDL_QUIT)
		{
			gRunning = false;
		}
		//Handle keypress with current mouse position
		else if (eventHandler.type == SDL_TEXTINPUT)
		{
			int x = 0, y = 0;
			SDL_GetMouseState(&x, &y);
			handleKeys(eventHandler.text.text[0], x, y);
		}
	}
}

void update()
{
	if (gAutomaticRotation == true)
	{
		rotateCamera(glm::vec3(0.0f, 0.02f, 0.0f));
	}
}

void render()
{
	//Clear color buffer
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Render quad
	if (gRenderQuad)
	{
		//Bind program
		glUseProgram(gProgramID);

		glBindVertexArray(gVAO);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gIBO);

		// Send our transformation to the currently bound shader, 
		// in the "MVP" uniform
		glUniformMatrix4fv(gMatrixID, 1, GL_FALSE, glm::value_ptr(gMVPMatrix));	//Note that glm::value_ptr(gMVPMatrix) is the same as &gMVPMatrix[0][0]

		//Actual drawing
		glDrawElements(GL_TRIANGLE_STRIP, num_vertices, GL_UNSIGNED_INT, NULL); // you can draw other elements here. GL_LINE_LOOP

		//Unbind program
		glBindVertexArray(0);
		glUseProgram(NULL);
	}

	//Update screen
	SDL_GL_SwapWindow(gWindow);
}

int main(int argc, char* args[])
{
	//Start up SDL and create window
	if (!init())
	{
		printf("Failed to initialize!\n");
	}
	else
	{
		//Main loop flag
		gRunning = true;

		//Event handler
		SDL_Event eventHandler;

		//Enable text input
		SDL_StartTextInput();

		//While application is running
		while (gRunning)
		{
			//Handle events
			events(eventHandler);

			//Handle updates
			update();

			//Render quad
			render();
		}

		//Disable text input
		SDL_StopTextInput();
	}

	//Free resources and close SDL
	close();

	return 0;
}