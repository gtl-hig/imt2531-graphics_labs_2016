#include "globals.h"

//Screen dimension constants
const int SCREEN_WIDTH = 640;
const int SCREEN_HEIGHT = 480;

//The window we'll be rendering to
SDL_Window* gWindow = NULL;

//OpenGL context
SDL_GLContext gContext;

//Program is running?
bool gRunning = true;

//Render flag
bool gRenderQuad = true;
bool gAutomaticRotation = false;

//Graphics program
GLuint gProgramID = 0;
GLuint gVBO = 0;
GLuint gIBO = 0;
GLuint gVAO = 0;

//Model data
int num_vertices=0;
int vertex_order = 4; // we are using x,y,z,w vertices
int color_depth = 4;  // 4 bytes is 32 bit color
int normal_order = 4;

int num_indices = 0;

//Matrixes used to switch between coordinate systems
GLuint gMatrixID = 0;
GLuint gViewMatrixID = 0;
GLuint gModelMatrixID = 0;

glm::mat4 gModelMatrix;
glm::mat4 gProjectionMatrix;
glm::mat4 gViewMatrix;

glm::mat4 gMVPMatrix;

//Camera
glm::vec3 gCameraPosition = glm::vec3(0.0f, 0.0f, 30.0f);