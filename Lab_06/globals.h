#pragma	once

#if defined(__linux__)						// If we are using linux.

#include <SDL2/SDL.h>
#include <GL/glew.h>
#include <SDL2/SDL_opengl.h>

#elif defined(_WIN16) || defined(_WIN32) || defined(_WIN64)	// If we are using a windows machine
#include <SDKDDKVer.h>

//Using SDL, SDL OpenGL, GLEW, standard IO, and strings
#include <SDL.h>
#include <gl\glew.h>
#include <SDL_opengl.h>
#include <gl\glu.h>
#endif

#define GLM_FORCE_RADIANS //Using degrees with glm is deprecated.
#include <glm/glm.hpp>
#include <glm/gtc/constants.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

//Screen dimension constants
extern const int SCREEN_WIDTH;
extern const int SCREEN_HEIGHT;

//The window we'll be rendering to
extern SDL_Window* gWindow;

//OpenGL context
extern SDL_GLContext gContext;

//Program is running?
extern bool gRunning;

//Render flag
extern bool gRenderQuad;
extern bool gAutomaticRotation;

//Graphics program
extern GLuint gProgramID;
extern GLuint gVBO;
extern GLuint gIBO;
extern GLuint gVAO;

//Model data
extern int num_vertices;
extern int vertex_order; // we are using x,y,z,w vertices
extern int color_depth;  // 4 bytes is 32 bit color
extern int normal_order;

extern int num_indices;

//Matrixes used to switch between coordinate systems
extern GLuint gMatrixID;
extern GLuint gViewMatrixID;
extern GLuint gModelMatrixID;

extern glm::mat4 gModelMatrix;
extern glm::mat4 gProjectionMatrix;
extern glm::mat4 gViewMatrix;

extern glm::mat4 gMVPMatrix;

//Camera
extern glm::vec3 gCameraPosition;