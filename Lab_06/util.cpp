#include "util.h"
#include "globals.h"

bool init()
{
	//Initialization flag
	bool success = true;

	//Initialize SDL
	if (SDL_Init(SDL_INIT_VIDEO) < 0)
	{
		printf("SDL could not initialize! SDL Error: %s\n", SDL_GetError());
		success = false;
	}
	else
	{
		//Ask SDL to give us a core context.
		SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);

		//Create window
		gWindow = SDL_CreateWindow("SDL Tutorial", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN);
		if (gWindow == NULL)
		{
			printf("Window could not be created! SDL Error: %s\n", SDL_GetError());
			success = false;
		}
		else
		{
			//Create context
			gContext = SDL_GL_CreateContext(gWindow);
			if (gContext == NULL)
			{
				printf("OpenGL context could not be created! SDL Error: %s\n", SDL_GetError());
				success = false;
			}
			else
			{
				//Initialize GLEW
				glewExperimental = GL_TRUE;
				GLenum glewError = glewInit();
				if (glewError != GLEW_OK)
				{
					printf("Error initializing GLEW! %s\n", glewGetErrorString(glewError));
				}

				//Use Vsync
				if (SDL_GL_SetSwapInterval(1) < 0)
				{
					printf("Warning: Unable to set VSync! SDL Error: %s\n", SDL_GetError());
				}

				initGL();			
			}
		}
	}

	return success;
}

void initGL()
{
	// Enable depth test
	glEnable(GL_DEPTH_TEST);
	// Accept fragment if it closer to the camera than the former one
	glDepthFunc(GL_LESS);

	// Projection matrix : 45° (quarter_pi) Field of View, screen size as the ratio, display range : 0.1 unit <-> 100 units
	gProjectionMatrix = glm::perspective(glm::quarter_pi<float>(), static_cast<float>(SCREEN_WIDTH) / static_cast<float>(SCREEN_HEIGHT), 0.1f, 100.0f);

	// Camera matrix
	gViewMatrix = glm::lookAt(
		gCameraPosition, // Camera position in World Space
		glm::vec3(0.0f, 0.0f, 0.0f), // and looks at the origin
		glm::vec3(0.0f, 1.0f, 0.0f)  // Head is up (set to 0,-1,0 to look upside-down)
		);

	gModelMatrix = glm::mat4(1.0f);

	// Our ModelViewProjection : multiplication of our 3 matrices
	gMVPMatrix = gProjectionMatrix * gViewMatrix * gModelMatrix; // Remember, matrix multiplication is the other way around

	//Generate program
	gProgramID = glCreateProgram();
	gProgramID = LoadShaders("Color.vs", "Color.fs");

	glUseProgram(gProgramID);

	//Initialize clear color
	glClearColor(0.f, 0.f, 0.f, 1.f);

	initBuffers();
}

void initBuffers() {
	// Since we are asking for a core context, we also need to create and bind a VAO.
	glGenVertexArrays(1, &gVAO);
	glBindVertexArray(gVAO);

	//IBO data
	num_indices = 14;
	GLuint indexData[] = { 0, 1, 4, 5, 6, 1, 2, 0, 3, 4, 7, 6, 3, 2 };
	//Create IBO
	glGenBuffers(1, &gIBO);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gIBO);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indexData) * sizeof(GLuint), indexData, GL_STATIC_DRAW);

	//VBO data
	num_vertices = 8;
	GLfloat vertexData[] =
	{
		//vertex
		-5.0f, -5.0f, 5.0f, 1.0f,
		5.0f, -5.0f, 5.0f, 1.0f,
		5.0f, 5.0f, 5.0f, 1.0f,
		-5.0f, 5.0f, 5.0f, 1.0,

		-5.0f, -5.0f, -5.0f, 1.0f,
		5.0f, -5.0f, -5.0f, 1.0f,
		5.0f, 5.0f, -5.0f, 1.0f,
		-5.0f, 5.0f, -5.0f, 1.0,

		//color
		1.0f, 0.0f, 0.0f, 1.0f,
		0.0f, 1.0f, 0.0f, 1.0f,
		0.0f, 0.0f, 1.0f, 1.0f,
		0.0f, 1.0f, 1.0f, 1.0f,

		1.0f, 0.0f, 0.0f, 1.0f,
		0.0f, 1.0f, 0.0f, 1.0f,
		0.0f, 0.0f, 1.0f, 1.0f,
		0.0f, 1.0f, 1.0f, 1.0f
	};

	//Create VBO
	glGenBuffers(1, &gVBO);
	glBindBuffer(GL_ARRAY_BUFFER, gVBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertexData) * sizeof(GLfloat), vertexData, GL_STATIC_DRAW);

	//Enable vertex position
	glEnableVertexAttribArray(0); //attribute 0 is for vertex position data
	glEnableVertexAttribArray(1); //attribute 1 is for vertex color data
	glEnableVertexAttribArray(2); //attribute 2 is for normals

	//Set vertex data
	glVertexAttribPointer(0, vertex_order, GL_FLOAT, GL_FALSE, vertex_order* sizeof(GLfloat), NULL);
	glVertexAttribPointer(1, color_depth, GL_FLOAT, GL_FALSE, color_depth* sizeof(GLfloat),  (void*)(num_vertices*vertex_order*sizeof(GL_FLOAT)));//color data is 64 bytes in to the array
	glVertexAttribPointer(2, normal_order, GL_FLOAT, GL_FALSE, normal_order * sizeof(GLfloat), (void*)(num_vertices*vertex_order*sizeof(GL_FLOAT) + num_vertices * color_depth*sizeof(GL_FLOAT))); //normal data starts after the color data

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);
}

void handleKeys(unsigned char key, int x, int y)
{
	//Toggle quad
	if (key == 'q')
	{
		gRenderQuad = !gRenderQuad;
	}
	else if (key == 'r')
	{
		gAutomaticRotation = !gAutomaticRotation;
	}
}

void close()
{
	//Deallocate program
	glDeleteProgram(gProgramID);

	//Destroy window	
	SDL_DestroyWindow(gWindow);
	gWindow = NULL;

	//Quit SDL subsystems
	SDL_Quit();
}

void printProgramLog(GLuint program)
{
	//Make sure name is shader
	if (glIsProgram(program))
	{
		//Program log length
		int infoLogLength = 0;
		int maxLength = infoLogLength;

		//Get info string length
		glGetProgramiv(program, GL_INFO_LOG_LENGTH, &maxLength);

		//Allocate string
		char* infoLog = new char[maxLength];

		//Get info log
		glGetProgramInfoLog(program, maxLength, &infoLogLength, infoLog);
		if (infoLogLength > 0)
		{
			//Print Log
			printf("%s\n", infoLog);
		}

		//Deallocate string
		delete[] infoLog;
	}
	else
	{
		printf("Name %d is not a program\n", program);
	}
}

void printShaderLog(GLuint shader)
{
	//Make sure name is shader
	if (glIsShader(shader))
	{
		//Shader log length
		int infoLogLength = 0;
		int maxLength = infoLogLength;

		//Get info string length
		glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &maxLength);

		//Allocate string
		char* infoLog = new char[maxLength];

		//Get info log
		glGetShaderInfoLog(shader, maxLength, &infoLogLength, infoLog);
		if (infoLogLength > 0)
		{
			//Print Log
			printf("%s\n", infoLog);
		}

		//Deallocate string
		delete[] infoLog;
	}
	else
	{
		printf("Name %d is not a shader\n", shader);
	}
}