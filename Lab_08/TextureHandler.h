//
//  TextureHandler.h
//  Lab06
//
//  Created by Sporaland on 09/10/14.
//  Copyright (c) 2014 IMT2531. All rights reserved.
//

#ifndef __TextureHandler__
#define __TextureHandler__

#if defined(__linux__)						// If we are using linux.
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_opengl.h>
#include <string>

#elif defined(_WIN16) || defined(_WIN32) || defined(_WIN64)	// If we are using a windows machine
#include <SDL_image.h>
#include <SDL_opengl.h>
#include <string>
#endif

/**
 * Used to handle the loading of textures for use with OpenGL.
 */
class TextureHandler
{
public:
	/**
	 * Loads an image and greates an OpenGL texture buffer.
	 * @param imageName The path to the image we are loading.
	 * @returns The texture identifier for use with OpenGL.
	 */
	GLuint createTextureFromImage(const std::string& imageName);

	/**
	 * Creates a 2D texture with the specified size and filtering.
	 * @param textureWidth The width of the texture.
	 * @param textureHeight The height of the texture.
	 * @param filter specifies the filtering method to be used, should be GL_LINEAR or GL_NEAREST.
	 */
	GLuint createTexture(const GLsizei textureWidth, const GLsizei textureHeight, const GLint filtering = GL_NEAREST);

private:

	GLenum getCorrectTexelFormat(const SDL_Surface* textureSurface);
	
};
#endif /* defined(__Lab04__TextureHandler__) */
