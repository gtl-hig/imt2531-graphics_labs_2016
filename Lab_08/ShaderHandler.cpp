#include "ShaderHandler.h"

const std::string shaderPath = "../resources/shaders/";

ShaderHandler::ShaderProgram* ShaderHandler::initializeShaders()
{
	this->shaders["Red"] = std::unique_ptr<ShaderProgram>(new ShaderProgram("Red"));
	this->shaders["Color"] = std::unique_ptr<ShaderProgram>(new ShaderProgram("Color"));
	this->shaders["Texture"] = std::unique_ptr<ShaderProgram>(new ShaderProgram("Texture"));
	this->shaders["TextureAndColor"] = std::unique_ptr<ShaderProgram>(new ShaderProgram("TextureAndColor"));
	this->shaders["DiffuseLighting"] = std::unique_ptr<ShaderProgram>(new ShaderProgram("DiffuseLighting"));
	this->shaders["FullLighting"] = std::unique_ptr<ShaderProgram>(new ShaderProgram("FullLighting"));
	this->shaders["FullLightingNoTextureOrColor"] = std::unique_ptr<ShaderProgram>(new ShaderProgram("FullLightingNoTextureOrColor"));
	this->shaders["DiffuseLightingNoTextureOrColor"] = std::unique_ptr<ShaderProgram>(new ShaderProgram("DiffuseLightingNoTextureOrColor"));

	return this->shaders["FullLighting"].get(); 
}

ShaderHandler::ShaderProgram* ShaderHandler::getShader(const std::string& shader)
{
	auto it = this->shaders.find(shader);
	if (it != this->shaders.end())
	{
		return this->shaders[shader].get();
	}

	return nullptr;
}

ShaderHandler::ShaderProgram::ShaderProgram(const std::string& shader)
{
	const std::string vertexSuffix = ".vs";
	const std::string fragmentSuffix = ".fs";

	this->programId = LoadShaders((shaderPath + shader + vertexSuffix).c_str(), (shaderPath + shader + fragmentSuffix).c_str());
	
	this->MVPId = glGetUniformLocation(this->programId, "MVP");

	this->viewMatrixId = glGetUniformLocation(this->programId, "ViewMatrix");
	this->modelMatrixId = glGetUniformLocation(this->programId, "ModelMatrix");

	this->textureId = glGetUniformLocation(this->programId, "textureBuffer");

	this->lightPositionId = glGetUniformLocation(this->programId, "LightPosition_worldspace");

	this->cameraPositionId = glGetUniformLocation(this->programId, "CameraPosition_worldspace");


	this->lightColorId = glGetUniformLocation(this->programId, "lightColor");
	this->materialDiffuseColorId = glGetUniformLocation(this->programId, "materialDiffuseColor");
	this->materialSpecularColorId = glGetUniformLocation(this->programId, "materialSpecularColor");

	glUseProgram(this->programId);
}

ShaderHandler::ShaderProgram::~ShaderProgram()
{
	glDeleteProgram(this->programId);
}