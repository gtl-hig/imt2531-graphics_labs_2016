#pragma	once

#if defined(__linux__)						// If we are using linux.

#include <SDL2/SDL.h>
#include <GL/glew.h>
#include <SDL2/SDL_opengl.h>

#elif defined(_WIN16) || defined(_WIN32) || defined(_WIN64)	// If we are using a windows machine
#include <SDKDDKVer.h>

//Using SDL, SDL OpenGL, GLEW, standard IO, and strings
#include <SDL.h>
#include <gl\glew.h>
#include <SDL_opengl.h>
#include <gl\glu.h>
#endif

#define GLM_FORCE_RADIANS //Using degrees with glm is deprecated.
#include <glm/glm.hpp>
#include <glm/gtc/constants.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include "ShaderHandler.h"
#include "TextureHandler.h"

//Screen dimension constants
extern const int SCREEN_WIDTH;
extern const int SCREEN_HEIGHT;

//The window we'll be rendering to
extern SDL_Window* gWindow;

//OpenGL context
extern SDL_GLContext gContext;

//Render flags
extern bool gRunning;
extern bool gRenderQuad;
extern bool gAutomaticRotation;

//Graphics program
extern GLuint gProgramID;

//Buffer adresses
extern GLuint gVAOCube;
extern GLuint gVBOCube;
extern GLuint gIBOCube;

extern GLuint gVAOFloor;
extern GLuint gVBOFloor;
extern GLuint gIBOFloor;

//Model data
extern int num_vertices;
extern int vertex_order; // we are using x,y,z,w vertices
extern int color_depth;  // 4 bytes is 32 bit color
extern int normal_order;
extern int texture_order;

extern int num_indices;

//Matrixes//////////////////////////////////////////////////////////
//Model
extern glm::mat4 gModelMatrixCube;
extern glm::mat4 gModelMatrixFloor;

//View
extern glm::mat4 gViewMatrix;

//Projection
extern glm::mat4 gProjectionMatrix;

//MVP
extern glm::mat4 gMVPMatrixCube;
extern glm::mat4 gMVPMatrixFloor;
////////////////////////////////////////////////////////////////////

//Camera
extern glm::vec3 gCameraPosition;

//Light
extern glm::vec3 gLightPosition;
extern glm::vec4 gLightColor;

//Shaders
extern ShaderHandler shaderHandler;
extern ShaderHandler::ShaderProgram* currentShaderProgram;

//Textures
extern TextureHandler gTextureHandler;
extern GLuint gTextureBuffer;

//Material Data Cube
extern glm::vec4 gMaterialCubeDiffuseColor;
extern glm::vec4 gMaterialCubeSpecularColor;

//Material Data Floor
extern glm::vec4 gMaterialFloorDiffuseColor;
extern glm::vec4 gMaterialFloorSpecularColor;