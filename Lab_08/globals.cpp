#include "globals.h"

//Screen dimension constants
const int SCREEN_WIDTH = 720;
const int SCREEN_HEIGHT = 720;

//The window we'll be rendering to
SDL_Window* gWindow = NULL;

//OpenGL context
SDL_GLContext gContext;

//Render flags
bool gRunning = true;
bool gRenderQuad = true;
bool gAutomaticRotation = false;

//Graphics program
GLuint gProgramID = 0;

//Buffer adresses
GLuint gVAOCube = 0;
GLuint gVBOCube = 0;
GLuint gIBOCube = 0;

GLuint gVAOFloor = 0;
GLuint gVBOFloor = 0;
GLuint gIBOFloor = 0;

//Model data
int num_vertices=0;
int vertex_order = 4; // we are using x,y,z,w vertices
int color_depth = 4;  // 4 bytes is 32 bit color
int texture_order = 4;
int normal_order = 4;
int num_indices = 0;

//Matrixes
//Model
glm::mat4 gModelMatrixCube;
glm::mat4 gModelMatrixFloor;

//View
glm::mat4 gViewMatrix;

//Projection
glm::mat4 gProjectionMatrix;

//MVP
glm::mat4 gMVPMatrixCube;
glm::mat4 gMVPMatrixFloor;

//Camera
glm::vec3 gCameraPosition = glm::vec3(0.0f, 10.0f, -40.0f);

//Light
glm::vec3 gLightPosition = gCameraPosition;
glm::vec4 gLightColor = glm::vec4(1.0f, 1.0f, 1.0f, 10.0f);

//Shaders
ShaderHandler shaderHandler;
ShaderHandler::ShaderProgram* currentShaderProgram;

//Textures
TextureHandler gTextureHandler;
GLuint gTextureBuffer = 0;

//Material Data Cube
glm::vec4 gMaterialCubeDiffuseColor = glm::vec4(1.0f, 0.5f, 0.5f, 1.0f);
glm::vec4 gMaterialCubeSpecularColor = glm::vec4(1.0f, 1.0f, 1.0f, 1.0f);

//Material Data Floor
glm::vec4 gMaterialFloorDiffuseColor = glm::vec4(1.0f, 1.0f, 1.0f, 1.0f);
glm::vec4 gMaterialFloorSpecularColor = glm::vec4(1.0f, 1.0f, 1.0f, 1.0f);