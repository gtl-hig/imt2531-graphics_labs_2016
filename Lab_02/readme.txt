IMT2531 Lab 2
-----------------
Some of the files within this lab has been originally taken and modified from 
Lazy Foo' Productions (2004-2013).


Lab tasks
----------------------------------------

1. Currently the project is neither C++14, nor proper C-style. 
Tidy up the code and convert it so that it follows the C++14 style. 
Pay attention to coding conventions, naming, the use of 'auto',
'range-for loops', 'new/delete' and references. No magic numbers.


2. The character left-movement is currently not working, ie. responding to 
'a' key. Fix it.

3. The character can currently walk off-screen. Make sure that this is not 
possible, and that the character stays on the visible part of the window.

4. When the character is stationary, the walk animation continues to play. 
When the character doesn't move, a stationary animation, or fixed image should 
be shown.




This project is linked against:
----------------------------------------

SDL2, SDL2main
https://www.libsdl.org/download-2.0.php


SDL2_image
https://www.libsdl.org/projects/SDL_image/




