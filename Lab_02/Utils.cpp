#include "Utils.h"

//Create a 32 bit color surface
SDL_Surface* create_surface32(int width, int height)
{
    // Create a 32-bit surface with the bytes of each pixel in R,G,B,A order,
    //   as expected by OpenGL for textures
    Uint32 rmask, gmask, bmask, amask;

    // SDL interprets each pixel as a 32-bit number, so our masks must depend
    //   on the endianness (byte order) of the machine
#if SDL_BYTEORDER == SDL_BIG_ENDIAN
    rmask = 0xff000000;
    gmask = 0x00ff0000;
    bmask = 0x0000ff00;
    amask = 0x000000ff;
#else
    rmask = 0x000000ff;
    gmask = 0x0000ff00;
    bmask = 0x00ff0000;
    amask = 0xff000000;
#endif

    return SDL_CreateRGBSurface(SDL_SWSURFACE, width, height, 32, rmask, gmask, bmask, amask);
}

Uint32 color32(const Uint8 r, const Uint8 g, const Uint8 b, const Uint8 alpha)
{
    return (Uint32(alpha) << 24) + (Uint32(r) << 16) + (Uint32(g) << 8) + Uint32(b);
}

void clear_surface(SDL_Surface *surface, const Uint8 r, const Uint8 g, const Uint8 b, const Uint8 alpha)
{
    SDL_FillRect(surface, NULL, color32(r, g, b, alpha));
}

void get_pixel(SDL_Surface *surface, int x, int y, Uint8 *r, Uint8 *g, Uint8 *b, Uint8 *a)
{
    Uint8 *pixel = (Uint8*)surface->pixels + ((y*surface->w) + x)*4;    //Get a pointer to the pixel
    //Get the BGRA pixel values
    *b = pixel[0];
    *g = pixel[1];
    *r = pixel[2];
    *a = pixel[3];
}

void set_pixel(SDL_Surface *surface, int x, int y, const Uint8 r, const Uint8 g, const Uint8 b, const Uint8 a)
{
    Uint8 *pixel = (Uint8*)surface->pixels + ((y*surface->w) + x)*4;    //Get a pointer to the pixel
    //set the BGRA pixel components
    pixel[0] = b;
    pixel[1] = g;
    pixel[2] = r;
    pixel[3] = a;
}
