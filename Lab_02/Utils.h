#pragma once

#include <SDL2/SDL.h>

//Create a 32 bit color surface
SDL_Surface* create_surface32(int width, int height);

//Clear a surface to a single color
void clear_surface(SDL_Surface *surface, const Uint8 r, const Uint8 g, const Uint8 b, const Uint8 alpha);

//Create a single 32 bit value containing the red, green, blue and alpha components
Uint32 color32(const Uint8 r, const Uint8 g, const Uint8 b, const Uint8 alpha);

//Direct pixel access functions. Quite slow
void get_pixel(SDL_Surface *surface, int x, int y, Uint8 *r, Uint8 *g, Uint8 *b, Uint8 *a);
void set_pixel(SDL_Surface *surface, int x, int y, const Uint8 r, const Uint8 g, const Uint8 b, const Uint8 a);
