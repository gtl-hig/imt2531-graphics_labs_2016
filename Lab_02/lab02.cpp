/*This source code copyrighted by Lazy Foo' Productions (2004-2013)
and may not be redistributed without written permission.*/

//Using SDL and standard IO
#include <SDL2/SDL.h>
#include <stdio.h>
#include <SDL2/SDL_image.h>
#include <iostream>
#include <string>

//Screen dimension constants
const int SCREEN_WIDTH = 640;
const int SCREEN_HEIGHT = 480;
const int DIRECTIONS = 8;
const int WALK_SPEED = 5;
//Starts up SDL and creates window
bool init();

std::string Simon;
//Loads media
bool loadMedia();

//Frees media and shuts down SDL
void close();

//The window we'll be rendering to
SDL_Window* gWindow = NULL;
	
//The surface contained by the window
SDL_Surface* gScreenSurface = NULL;

//The image we will load and show on the screen
SDL_Surface* gHelloWorld = NULL;
SDL_Surface* spacemanImages[DIRECTIONS];


bool init()
{
	//Initialization flag
	bool success = true;

	//Initialize SDL
	if( SDL_Init( SDL_INIT_VIDEO ) < 0 )
	{
		printf( "SDL could not initialize! SDL_Error: %s\n", SDL_GetError() );
		success = false;
	}
	else
	{
		//Create window
		gWindow = SDL_CreateWindow( "SDL Tutorial", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN );
		if( gWindow == NULL )
		{
			printf( "Window could not be created! SDL_Error: %s\n", SDL_GetError() );
			success = false;
		}
		else
		{
			//Get window surface
			gScreenSurface = SDL_GetWindowSurface( gWindow );
		}
	}

	return success;
}

bool loadMedia(std::string filename, SDL_Surface ** destination)
{
	//Loading success flag
	bool success = true;

	//Load splash image
	*destination = IMG_Load(filename.c_str());
	if( *destination == NULL )
	{
		printf("Unable to load image %s! SDL Error: %s\n", filename.c_str(), SDL_GetError());
		success = false;
	}

	return success;
}

void close()
{
	//Deallocate surface
	SDL_FreeSurface( gHelloWorld );
	gHelloWorld = NULL;

	//Destroy window
	SDL_DestroyWindow( gWindow );
	gWindow = NULL;

	//Quit SDL subsystems
	SDL_Quit();
}



void handleKeys(SDL_Event* event, int *dir, int *vel){
	if (event->type == SDL_KEYDOWN){
		switch (event->key.keysym.sym){
		case SDLK_w: 
			*dir = 4;
			*vel = WALK_SPEED;
			break;
		case SDLK_s: 
			*dir = 0;
			*vel = WALK_SPEED;
			break;
		case SDLK_d: 
			*dir = 2;
			*vel = WALK_SPEED;
			break;
		}
		//more needed in here, and you might want to have x and y separate, and make a player object etc.
	} else {
		if (event->type == SDL_KEYUP){
			*vel = 0;
		}
	}
}


int main( int argc, char* args[])
{
	std::string spacemanBaseFilename = "spacemanLarge/spaceman_000";
	std::string spacemanFilename;

	int spacemanMovement = 0;
	int spacemanVelocity = 0;

	//Start up SDL and create window
	if( !init() )
	{
		printf( "Failed to initialize!\n" );
	}
	else
	{
		//Load media
		if (!loadMedia("SpaceBackground.jpg", &gHelloWorld))
		{
			printf( "Failed to load media!\n" );
		}
		else
		{
			for (int i = 0; i < DIRECTIONS; i++){
				spacemanFilename = spacemanBaseFilename;
				spacemanFilename.append(std::to_string(i));
				spacemanFilename.append(".png");
				loadMedia(spacemanFilename, &spacemanImages[i]);
			}

			//Apply the image
			SDL_BlitSurface( gHelloWorld, NULL, gScreenSurface, NULL );
			//Update the surface
			SDL_UpdateWindowSurface( gWindow );

		}

		SDL_Event event;

		SDL_Rect sourceRect;
		SDL_Rect destRect;

		int quit=0;
		bool left = false;

		destRect.x = 30;
		destRect.y = 30;
		destRect.h = 100;
		destRect.w = 100;

		sourceRect.x = 0;
		sourceRect.y = 0;
		sourceRect.h = 100;
		sourceRect.w = 100;


		while (!quit)
		{

			SDL_BlitSurface(gHelloWorld, NULL, gScreenSurface, NULL);

			while (SDL_PollEvent(&event))
			{
				if (event.type == SDL_QUIT){
					quit = 1;
				}
				if (event.type == SDL_KEYDOWN || event.type== SDL_KEYUP){
					handleKeys(&event, &spacemanMovement, &spacemanVelocity);
				}
			}
				// this should only be done when moving.
				sourceRect.x += 100;
				if (sourceRect.x >= 600){
					sourceRect.x = 0;
					sourceRect.y += 100;
				}
				if (sourceRect.y >= 500){
					sourceRect.y = 0;
				}


				if (spacemanVelocity > 0){
					switch (spacemanMovement){
					case 2:destRect.x += spacemanVelocity;
						break;
					case 4:destRect.y -= spacemanVelocity;
						break;
					case 0:destRect.y += spacemanVelocity;
						break;
					}
				}

				SDL_BlitSurface(spacemanImages[spacemanMovement], &sourceRect, gScreenSurface, &destRect);
				SDL_Delay(50);
				SDL_UpdateWindowSurface(gWindow);

		
		}

	}

	//Free resources and close SDL
	close();

	return 0;
}